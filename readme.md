#Snakefile

This snakefile is a metagenomics snakemake pipeline that filters metagenomic data, and blasts them against the bacterial database

##Installation

You will need the following programs to run the files:

Python version 3.6 or higher

Snakemake version 5.4.4 or higher

Trimmomatic version 0.39 or higher

Fastqc version 0.11.8 or higher

MetaSPAdes version 3.7 or higher

BLAST package and locally installed bac_ref_nt database containing bacterial nucleotide reference data

##Usage

Run the script using the following shell command:
"""
snakemake --snakefile Snakefile
"""

Subset data of metagenomics patient feces data is available under the Data directory

##Troubleshooting

Ensure that any locally needed files such as your bac_ref_nt database have their paths properly edited in the Snakefile

Consult the log files stored in the Logs directory to see errors for specific rules

##Contributing

This repository and its contents are publicly available and you are free to clone and use the code

##Reference

This repository is made by Wouter Jan Schuiten of the Hanzehogeschool Groningen Institute of Life Sciences and Technology

contact: w.j.schuiten@st.hanze.nl
