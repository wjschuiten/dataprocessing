import glob

configfile: "config.yaml"

def createinterleavewildcards(wildcards):
    #Returns R1 and R2 wildcards for rules that have to run both in their own shell command
    return sorted(expand(
        "Data/{interleave}",
        interleave=config['interleaves']))


rule all:
    input:
        #Rule all: contains all final pathway output files
        expand('Data/fastqc/{fastqc}_fastqc.html', fastqc = ['forward_paired', 'reverse_paired', 'R1', 'R2']),
        'Data/results/filtered_results.out',
        'Data/results/count_barplot.jpg'

rule deinterleave:
    #Splits the two frames of an interleaved paired-end file
    #Input: interleaved paired end file
    #Output: R1 and R2 fasta files of the two frames
    input:
        expand(sorted(glob.glob(config['data'])))
    output:
        sorted(expand('Data/{interleave}raw.fq', interleave = config['interleaves']))
    log:
        'Logs/deinterleave.log'
    shell:
        'Scripts/deinterleave_fastq.sh < {input} {output} 2> {log}'

rule remove_newlines:
    #Filters trailing newlines at end of file
    #Input: fasta file
    #Output: fasta file with all empty lines removed
    input:
        "Data/{createinterleavewildcards}raw.fq"
    output:
        "Data/{createinterleavewildcards}.fq"
    log:
        'Logs/remove_newlines{createinterleavewildcards}.log'
    shell:
        "sed '/^[[:space:]]*$/d' {input} >> {output} 2> {log}"
        

rule zip_input:
    #Zips fasta files as .fq.gz archive
    #Input: fasta file
    #Output: archived fq.gz
    input:
        'Data/{createinterleavewildcards}.fq'
    output:
        'Data/{createinterleavewildcards}.fq.gz'
    log:
        'Logs/zip_input{createinterleavewildcards}.log'
    shell:
        'gzip {input} 2> {log}'


rule trimmomatic:
    #Trims reads, removes illumina adapters
    #Change LEADING TRAILING SLIDINGWINDOW and MINLEN settings to change sensitivity and tresholds
    #Input: .fq.gz archive of deinterleaved fasta file
    #Output: .fq.gz archive of trimmed reads
    input:
        sorted(expand('Data/{interleaves}.fq.gz', interleaves = config['interleaves']))
    output:
        sorted(expand('Data/{frame}.fq.gz', frame = config['frames']))
    log:
        'Logs/trimmomatic.log'
    shell:
        'java -jar Scripts/trimmomatic-0.38.jar PE -phred33 {input} {output} ILLUMINACLIP:Dependencies/TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:3:15 MINLEN:8 2> {log}'

rule fastqc:
    #Runs fastqc to determine read quality
    #Input: .fq.gz fasta files
    #Output: .zip and .html fastqc files
    input:
        sorted(expand('Data/{fastqc}.fq.gz', fastqc = config['fastqc']))
    output:
        sorted(expand('Data/fastqc/{fastqc}_fastqc.html', fastqc = config['fastqc'])),
        sorted(expand('Data/fastqc/{fastqc}_fastqc.zip', fastqc = config['fastqc']))
    log:
        'Logs/fastqc.log'
    shell:
        'fastqc {input} -o Data/fastqc 2> {log}'
        
rule metaspades:
    #Metagenomic assembly using spades
    #Input: forward paired and reverse paired read files
    #Output: Contigs in fasta format
    input:
        forward = 'Data/forward_paired.fq.gz',
        reverse = 'Data/reverse_paired.fq.gz'
    output:
        'Data/metaspades/contigs.fasta'
    log:
        'Logs/metaspades.log'
    shell:
        'metaspades -1 {input.forward} -2 {input.reverse} -o Data/metaspades 2> {log}'
        
rule find_orfs:
    #Finds the open reading frames in the contigs
    #Input: fasta file containing contigs
    #Output: fasta file containing ORFs
    input:
        'Data/metaspades/contigs.fasta'
    output:
        'Data/orf_output.fasta'
    log:
        'Logs/find_orfs.log'
    shell:
        'python3 Scripts/readMultifasta.py 2> {log}'
        
rule run_blast:
    #Executes blastn over file
    #Input: fasta file containing ORFs
    #Output: blastn results in tabular format
    input:
        'Data/orf_output.fasta'
    output:
        'Data/results/results.out'
    log:
        'Logs/run_blast.log'
    shell:
        "blastn -db /data/datasets/bac_ref_nt/bac_ref_nt -query Data/orf_output.fasta -out Data/results/results.out -outfmt '6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore stitle' 2> {log}"

rule filter_blast:
    #Filters blast results on e-value and identity
    #Input: blastn results in tabular format
    #Output: filtered results file in tabular format
    input:
        'Data/results/results.out'
    output:
        'Data/results/filtered_results.out'
    log:
        log1 = 'Logs/filter_blast_ev.log',
        log2 = 'Logs/filter_blast_id.log'
    shell:
        """
        python3 Scripts/blast_filter.py -I Data/results/results.out -O Data/results/filtered_resultstmp.out -c 11 -t 1e-25 2> {log.log1}
        python3 Scripts/blast_filter.py -I Data/results/filtered_resultstmp.out -O Data/results/filtered_results.out -c 3 -t 95 -L 2> {log.log2}
        rm Data/results/filtered_resultstmp.out
        rm Data/*raw.fq
        """
rule make_barplot:
    #Executes an R script to create a barplot of count data
    #Input: R1 or R2 archive, contig fasta file and orf fasta file
    #Output: Barplot of count data
    input:
        reads = 'Data/R1.fq.gz',
        contigs = 'Data/metaspades/contigs.fasta',
        orfs = 'Data/orf_output.fasta'
    output:
        'Data/results/count_barplot.jpg'
    shell:
        "Rscript Scripts/count_barplot.R {input.reads} {input.contigs} {input.orfs} {output}"
